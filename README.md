# amoCRM

## Для чего

Пакет позволяет быстро начать взаимодействие с [`API amoCRM`](https://www.amocrm.ru/developers/content/crm_platform/api-reference) без необходимости долгого изучения их документации.

В пакете представлен основной функционал для работы с такими сущностями как:
- Сделки: получение, создание, комплексное создание с контактом и компанией, редактирование;
- Воронки: получение;
- Контакты: получение, создание, редактирование;
- Примечания: создание;
- Задачи: получение, создание, редактирование, выполнение;
- Пользователи: получение.

## Функционал

### AmoClient

`AmoClient` - класс для работы с методами и операциями `API amoCRM`. При использовании его в проекте, рекомендуется инициализировать и зарегистрировать данный класс как `Singleton`. 

Пакет дает возможность использовать свои кастомные модели и запросы при обращении к необходимым методам.

Ответ возвращается в виде OperationResult, который в случае неудачи возвращает строку с описанием проблемы.

#### Пример использования AmoClient

```c#
var client = new AmoClient(
    "clientId",
    "clientSecret",
    "redirectUri",
    "subDomain",
    "authCode");

var operation = await client.GetPipelinesAsync(cancellationToken);
```

#### Operation

Также для сущностей, которые запрашиваются постранично, реализован абстрактный класс `Operation`.
`Operation` содержит:
- Поле `IsNextDataExists` - существует ли следующая страница;
- Метод `LoadNextPageAsync` - загружает следующую страницу;
- Метод `LoadAllPagesAsync` - загружает все страницы с первой до последней.

`Operation` реализуется получения для контактов, сделок, задач и пользователей.

#### Пример использования Operation

```c#
var leadOperation = client.CreateLeadOperation(withContacts: true);
var operation = await leadOperation.LoadAllPagesAsync(cancellationToken, 5);
```

## License
[LICENSE](/LICENSE)
