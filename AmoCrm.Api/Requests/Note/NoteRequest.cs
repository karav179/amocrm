﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Note;

public record NoteRequest
{
    [JsonProperty("entity_id")]
    public int EntityId { get; init; }

    [JsonProperty("created_by")]
    public int? CreatedBy { get; init; }

    [JsonProperty("note_type")]
    public string NoteType => "common";

    [JsonProperty("params")]
    public NoteParamsRequest Params { get; init; } = null!;
}
