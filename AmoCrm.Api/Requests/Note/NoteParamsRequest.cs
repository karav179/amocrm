﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Note;

public record NoteParamsRequest
{
    [JsonProperty("text")]
    public string Text { get; init; } = string.Empty;
}
