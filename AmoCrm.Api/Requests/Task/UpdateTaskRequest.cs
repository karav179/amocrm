﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Task;

public record UpdateTaskRequest : ModifiedTaskBaseRequest
{
    [JsonProperty("id")]
    public int Id { get; init; }
}
