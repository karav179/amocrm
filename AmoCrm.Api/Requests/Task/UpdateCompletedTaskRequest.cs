﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Task;

public record UpdateCompletedTaskRequest
{
    [JsonProperty("id")]
    public required int Id { get; init; }

    [JsonProperty("is_completed")]
    public bool IsCompleted => true;

    [JsonProperty("result")]
    public required TaskTextModel Result { get; init; }
}