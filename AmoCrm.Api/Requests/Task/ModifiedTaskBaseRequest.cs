﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Task;

public abstract record ModifiedTaskBaseRequest
{
    [JsonProperty("responsible_user_id")]
    public int? ResponsibleUserId { get; init; }

    [JsonProperty("entity_id")]
    public int? EntityId { get; init; }

    [JsonProperty("entity_type")]
    public string? EntityType { get; init; }

    [JsonProperty("is_completed")]
    public bool? IsCompleted { get; init; }

    [JsonProperty("task_type_id")]
    public int? TaskTypeId { get; init; }

    [JsonProperty("text")]
    public required string Text { get; init; }

    [JsonProperty("duration")]
    public int? Duration { get; init; }

    [JsonProperty("complete_till")]
    public required long CompleteTill { get; init; }

    [JsonProperty("created_by")]
    public int? CreatedBy { get; init; }

    [JsonProperty("updated_by")]
    public int? UpdatedBy { get; init; }

    [JsonProperty("created_at")]
    public long? CreateAt { get; init; }

    [JsonProperty("updated_at")]
    public long? UpdateAt { get; init; }

    [JsonProperty("result")]
    public TaskTextModel? Result { get; init; }
}
