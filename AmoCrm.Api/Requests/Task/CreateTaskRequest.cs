﻿namespace AmoCrm.Api.Requests.Task;

public record CreateTaskRequest : ModifiedTaskBaseRequest;
