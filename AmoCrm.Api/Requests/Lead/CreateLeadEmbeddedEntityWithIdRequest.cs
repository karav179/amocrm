﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public record CreateLeadEmbeddedEntityWithIdRequest
{
    [JsonProperty("id")]
    public int Id { get; init; }
}
