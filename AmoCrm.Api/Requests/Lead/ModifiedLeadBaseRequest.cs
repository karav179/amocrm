﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public abstract record ModifiedLeadBaseRequest
{
    [JsonProperty("name")]
    public string? Name { get; init; }

    [JsonProperty("price")]
    public int? Price { get; init; }

    [JsonProperty("status_id")]
    public int? StatusId { get; init; }

    [JsonProperty("pipeline_id")]
    public int? PipelineId { get; init; }

    [JsonProperty("created_by")]
    public int? CreatedBy { get; init; }

    [JsonProperty("updated_by")]
    public int? UpdatedBy { get; init; }

    [JsonProperty("closed_at")]
    public long? ClosedAt { get; init; }

    [JsonProperty("created_at")]
    public long? CreatedAt { get; init; }

    [JsonProperty("updated_at")]
    public long? UpdatedAt { get; init; }

    [JsonProperty("responsible_user_id")]
    public int? ResponsibleUserId { get; init; }

    [JsonProperty("custom_fields_values")]
    public CustomFieldModel[]? CustomFields { get; init; }
}
