﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public record CreateLeadEmbeddedRequest
{
    [JsonProperty("contacts")]
    public CreateLeadEmbeddedEntityWithIdRequest[]? Contacts { get; init; }

    [JsonProperty("tags")]
    public CreateLeadEmbeddedEntityWithIdRequest[]? Tags { get; init; }
}
