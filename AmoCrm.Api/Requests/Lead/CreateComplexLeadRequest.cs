﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public record CreateComplexLeadRequest : ModifiedLeadBaseRequest
{
    [JsonProperty("_embedded")]
    public CreateComplexLeadEmbeddedRequest? Embedded { get; init; }
}
