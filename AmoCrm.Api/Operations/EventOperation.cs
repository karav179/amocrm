﻿using AmoCrm.Api.Extensions;
using AmoCrm.Api.Responses.Event;

namespace AmoCrm.Api.Operations;

public class EventOperation : OperationProvider<EventArrayResponse?>
{
    internal EventOperation(AmoClient amoClient, string query, CancellationToken ct) : base(amoClient.GetEventsAsync, query, ct)
    { }
}
