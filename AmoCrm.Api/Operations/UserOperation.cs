﻿using AmoCrm.Api.Extensions;
using AmoCrm.Api.Responses.User;

namespace AmoCrm.Api.Operations;

public class UserOperation : OperationProvider<UserArrayResponse?>
{
    internal UserOperation(AmoClient amoClient, string query, CancellationToken ct) : base(amoClient.GetUsersAsync, query, ct)
    { }
}
