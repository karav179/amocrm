﻿using AmoCrm.Api.Extensions;
using AmoCrm.Api.Responses.Lead;

namespace AmoCrm.Api.Operations;

public class LeadOperation : OperationProvider<LeadArrayResponse?>
{
    internal LeadOperation(AmoClient amoClient, string query, CancellationToken ct) : base(amoClient.GetLeadsAsync, query, ct)
    { }
}
