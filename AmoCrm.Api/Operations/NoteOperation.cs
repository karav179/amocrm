﻿using AmoCrm.Api.Extensions;
using AmoCrm.Api.Responses.Note;

namespace AmoCrm.Api.Operations;

public class NoteOperation : OperationProvider<NoteArrayResponse?>
{
    internal NoteOperation(AmoClient amoClient, string query, CancellationToken ct) : base(amoClient.GetNotesAsync, query, ct)
    { }
}
