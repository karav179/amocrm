﻿using AmoCrm.Api.Extensions;
using AmoCrm.Api.Responses.Task;

namespace AmoCrm.Api.Operations;

public class TaskOperation : OperationProvider<TaskArrayResponse?>
{
    internal TaskOperation(AmoClient amoClient, string query, CancellationToken ct) : base(amoClient.GetTasksAsync, query, ct)
    { }
}
