﻿using AmoCrm.Api.Operations;
using AmoCrm.Api.Responses.User;
using Newtonsoft.Json.Linq;
using System.Text;

namespace AmoCrm.Api.Extensions;

public static class UserClientExtension
{
    private static string BuildQuery(int page, int? limit, string? query)
    {
        var queryBuilder = new StringBuilder(query);
        queryBuilder.Append($"&page={page}");

        if (limit.HasValue)
            queryBuilder.Append($"&limit={limit.Value}");

        var userQuery = queryBuilder.ToString();
        if (userQuery.StartsWith("&"))
            userQuery = userQuery[1..];

        return userQuery;
    }

    public static async Task<JObject?> GetUsersAsJObjectAsync(this AmoClient client, string? query = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder("/api/v4/users");

        if (!string.IsNullOrEmpty(query) && !query.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(query);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<UserArrayResponse?> GetUsersAsync(this AmoClient client, string? query = null, CancellationToken cancellationToken = default)
    {
        var resp = await GetUsersAsJObjectAsync(client, query, cancellationToken);
        return resp?.ToObject<UserArrayResponse>();
    }

    public static async Task<UserArrayResponse?> GetUsersAsync(this AmoClient client, int page, int? limit = null, string? query = null, CancellationToken cancellationToken = default)
    {
        var userQuery = BuildQuery(page, limit, query);
        return await GetUsersAsync(client, userQuery, cancellationToken);
    }

    public static UserOperation CreateUserOperation(this AmoClient client, int? limit = null, string? query = null, CancellationToken cancellationToken = default)
    {
        return new UserOperation(client, BuildQuery(1, limit, query), cancellationToken);
    }
}
