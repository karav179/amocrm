﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Operations;
using AmoCrm.Api.Responses.Event;
using Newtonsoft.Json.Linq;
using System.Text;

namespace AmoCrm.Api.Extensions;

public static class EventClientExtension
{
    private static string BuildQuery(
        string? query,
        int? page,
        int? limit,
        int[] eventIds,
        PeriodModel? createdAtFilter,
        int[] createdByIds,
        EventEntityFilterModel? entityFilterModel,
        string[] types)
    {
        var queryBuilder = new StringBuilder(query);

        if (page.HasValue)
            queryBuilder.Append($"&page={page.Value}");

        if (limit.HasValue)
            queryBuilder.Append($"&limit={limit.Value}");

        if (eventIds.Any())
            for (int i = 0; i < eventIds.Length; i++)
                queryBuilder.Append($"&filter[id][{i}]={eventIds[i]}");

        if (createdAtFilter is not null)
        {
            if (createdAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[created_at][from]={createdAtFilter.FromTicks.Value}");

            if (createdAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[created_at][to]={createdAtFilter.ToTicks.Value}");
        }

        if (createdByIds.Any())
            for (int i = 0; i < createdByIds.Length; i++)
                queryBuilder.Append($"&filter[created_by][{i}]={createdByIds[i]}");

        if (entityFilterModel is not null && entityFilterModel.EntityIds.Any())
        {
            queryBuilder.Append($"&filter[entity_type]={entityFilterModel.EntityType}");
            for (int i = 0; i < entityFilterModel.EntityIds.Length; i++)
                queryBuilder.Append($"&filter[entity_id][{i}]={entityFilterModel.EntityIds[i]}");
        }

        if (types.Any())
            for (int i = 0; i < types.Length; i++)
                queryBuilder.Append($"&filter[type][{i}]={types[i]}");

        var eventQuery = queryBuilder.ToString();
        if (eventQuery.StartsWith("&"))
            eventQuery = eventQuery[1..];

        return eventQuery;
    }


    internal static async Task<EventArrayResponse?> GetEventsAsync(this AmoClient client, string query, CancellationToken ct)
    {
        var resp = await GetEventsAsJObjectAsync(client, query, ct);
        return resp?.ToObject<EventArrayResponse>();
    }

    public static async Task<JObject?> GetEventsAsJObjectAsync(this AmoClient client, string? filterQuery = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder("/api/v4/events");

        if (!string.IsNullOrEmpty(filterQuery) && !filterQuery.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(filterQuery);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<EventArrayResponse?> GetEventsAsync(this AmoClient client,
        string? query = null,
        int? page = null,
        int? limit = null,
        int[]? eventIds = null,
        PeriodModel? createdAtFilter = null,
        int[]? createdByIds = null,
        EventEntityFilterModel? entityFilterModel = null,
        string[]? types = null,
        CancellationToken cancellationToken = default)
    {
        var taskQuery = BuildQuery(query, page, limit,
            eventIds ?? Array.Empty<int>(),
            createdAtFilter,
            createdByIds ?? Array.Empty<int>(),
            entityFilterModel,
            types ?? Array.Empty<string>());

        var resp = await GetEventsAsJObjectAsync(client, taskQuery, cancellationToken);
        return resp?.ToObject<EventArrayResponse>();
    }


    public static EventOperation CreateEventOperation(this AmoClient client,
        string? query = null,
        int? limit = null,
        int[]? eventIds = null,
        PeriodModel? createdAtFilter = null,
        int[]? createdByIds = null,
        EventEntityFilterModel? entityFilterModel = null,
        string[]? types = null,
        CancellationToken cancellationToken = default)
    {
        return new EventOperation(client, BuildQuery(query, 1, limit,
            eventIds ?? Array.Empty<int>(),
            createdAtFilter,
            createdByIds ?? Array.Empty<int>(),
            entityFilterModel,
            types ?? Array.Empty<string>()),
            cancellationToken);
    }
}
