﻿using AmoCrm.Api.Responses.Pipeline;
using Newtonsoft.Json.Linq;

namespace AmoCrm.Api.Extensions;

public static class PipelineClientExtension
{
    public static async Task<JObject?> GetPipelinesAsJObjectAsync(this AmoClient client, CancellationToken cancellationToken = default)
    {
        return await client.GetAsync("/api/v4/leads/pipelines", cancellationToken: cancellationToken);
    }

    public static async Task<PipelineArrayResponse?> GetPipelinesAsync(this AmoClient client, CancellationToken cancellationToken = default)
    {
        var resp = await GetPipelinesAsJObjectAsync(client, cancellationToken);
        return resp?.ToObject<PipelineArrayResponse>();
    }
}
