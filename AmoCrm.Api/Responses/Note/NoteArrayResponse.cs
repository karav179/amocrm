﻿using AmoCrm.Api.Internal.JsonConverters;
using AmoCrm.Api.Internal.Responses;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Note;

[JsonConverter(typeof(JsonPathConverter))]
public record NoteArrayResponse : BaseArrayResponse
{
    [JsonProperty("_embedded.notes")]
    public NoteResponse[] Notes { get; init; } = null!;
}
