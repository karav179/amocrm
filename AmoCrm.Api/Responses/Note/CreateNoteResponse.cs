﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Note;

public record CreateNoteResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("entity_id")]
    public int EntityId { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
