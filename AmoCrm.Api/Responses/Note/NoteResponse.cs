﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Internal.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AmoCrm.Api.Responses.Note;

public record NoteResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("entity_id")]
    public int EntityId { get; init; }

    [JsonProperty("created_by")]
    public int CreatedBy { get; init; }

    [JsonProperty("updated_by")]
    public int UpdatedBy { get; init; }

    [JsonProperty("created_at")]
    public long CreatedAtJson { get; init; }
    public DateTime CreatedAtUtc => UnixTimestampConverter.Convert(CreatedAtJson);

    [JsonProperty("updated_at")]
    public long UpdatedAtJson { get; init; }
    public DateTime UpdatedAtUtc => UnixTimestampConverter.Convert(UpdatedAtJson);

    [JsonProperty("responsible_user_id")]
    public int ResponsibleUserId { get; init; }

    [JsonProperty("group_id")]
    public int GroupId { get; init; }

    [JsonProperty("note_type")]
    public string NoteType { get; init; } = string.Empty;

    [JsonProperty("params")]
    public JToken Params { get; init; } = null!;

    [JsonProperty("account_id")]
    public int AccountId { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
