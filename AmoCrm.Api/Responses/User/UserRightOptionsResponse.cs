﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.User;

public record UserRightOptionsResponse
{
    [JsonProperty("view")]
    public string View { get; init; } = string.Empty;

    [JsonProperty("edit")]
    public string Edit { get; init; } = string.Empty;

    [JsonProperty("add")]
    public string Add { get; init; } = string.Empty;

    [JsonProperty("delete")]
    public string Delete { get; init; } = string.Empty;

    [JsonProperty("export")]
    public string Export { get; init; } = string.Empty;
}
