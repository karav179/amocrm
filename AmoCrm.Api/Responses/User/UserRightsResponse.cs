﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.User;

public record UserRightsResponse
{
    [JsonProperty("leads")]
    public UserRightOptionsResponse Leads { get; init; } = null!;

    [JsonProperty("contacts")]
    public UserRightOptionsResponse Contacts { get; init; } = null!;

    [JsonProperty("companies")]
    public UserRightOptionsResponse Companies { get; init; } = null!;

    [JsonProperty("tasks")]
    public UserRightOptionsResponse Tasks { get; init; } = null!;

    [JsonProperty("mail_access")]
    public bool MailAccess { get; init; }

    [JsonProperty("catalog_access")]
    public bool CatalogAccess { get; init; }

    [JsonProperty("is_admin")]
    public bool IsAdmin { get; init; }

    [JsonProperty("is_free")]
    public bool IsFree { get; init; }

    [JsonProperty("is_active")]
    public bool IsActive { get; init; }

    [JsonProperty("group_id")]
    public int? GroupId { get; init; }

    [JsonProperty("role_id")]
    public int? RoleId { get; init; }
}
