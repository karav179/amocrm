﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.User;

public record UserResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("name")]
    public string Name { get; init; } = string.Empty;

    [JsonProperty("email")]
    public string Email { get; init; } = string.Empty;

    [JsonProperty("lang")]
    public string Language { get; init; } = string.Empty;

    [JsonProperty("rights")]
    public UserRightsResponse Rights { get; init; } = null!;

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
