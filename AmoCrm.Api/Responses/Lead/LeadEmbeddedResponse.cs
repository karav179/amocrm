﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

public record LeadEmbeddedResponse
{
    [JsonProperty("tags")]
    public LeadTagResponse[]? Tags { get; init; }

    [JsonProperty("contacts")]
    public LeadContactResponse[]? Contacts { get; init; }
}
