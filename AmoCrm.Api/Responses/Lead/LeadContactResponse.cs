﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

public record LeadContactResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("is_main")]
    public bool IsMain { get; init; }
}
