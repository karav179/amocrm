﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

public record CreateLeadResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("request_id")]
    public int RequestId { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
