﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

public record LeadTagResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("name")]
    public string Name { get; init; } = string.Empty;
}
