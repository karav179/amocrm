﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

public record UpdateLeadResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("updated_at")]
    public long UpdatedAt { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
