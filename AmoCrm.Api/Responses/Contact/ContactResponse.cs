﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Internal.Converters;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Contact;

public record ContactResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("name")]
    public string Name { get; init; } = string.Empty;

    [JsonProperty("first_name")]
    public string FirstName { get; init; } = string.Empty;

    [JsonProperty("last_name")]
    public string LastName { get; init; } = string.Empty;

    [JsonProperty("responsible_user_id")]
    public int ResponsibleUserId { get; init; }

    [JsonProperty("group_id")]
    public int GroupId { get; init; }

    [JsonProperty("created_by")]
    public int CreatedBy { get; init; }

    [JsonProperty("updated_by")]
    public int UpdatedBy { get; init; }

    [JsonProperty("created_at")]
    private long CreatedAtJson { get; init; }

    public DateTime CreatedAtUtc => UnixTimestampConverter.Convert(CreatedAtJson);

    [JsonProperty("updated_at")]
    public long UpdatedAtJson { get; init; }

    public DateTime UpdatedAtUtc => UnixTimestampConverter.Convert(UpdatedAtJson);

    [JsonProperty("is_deleted")]
    public bool IsDeleted { get; init; }

    [JsonProperty("closest_task_at")]
    public long? ClosestTaskAtJson { get; init; }

    public DateTime? ClosestTaskAtUtc => ClosestTaskAtJson.HasValue ? UnixTimestampConverter.Convert(ClosestTaskAtJson.Value) : null;

    [JsonProperty("account_id")]
    public int AccountId { get; init; }

    [JsonProperty("custom_fields_values")]
    public CustomFieldModel[]? CustomFields { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
