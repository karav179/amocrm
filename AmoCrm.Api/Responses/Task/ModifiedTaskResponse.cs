﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Task;

public record ModifiedTaskResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
