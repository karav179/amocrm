﻿using AmoCrm.Api.Internal.JsonConverters;
using AmoCrm.Api.Internal.Responses;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Task;

[JsonConverter(typeof(JsonPathConverter))]
public record TaskArrayResponse : BaseArrayResponse
{
    [JsonProperty("_embedded.tasks")]
    public TaskResponse[] Tasks { get; init; } = null!;
}
