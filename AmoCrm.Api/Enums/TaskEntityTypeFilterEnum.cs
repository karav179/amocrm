﻿namespace AmoCrm.Api.Enums;

public enum TaskEntityTypeFilterEnum
{
    leads,
    contacts,
    companies,
    customers
}
