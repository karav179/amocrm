﻿using AmoCrm.Api.Enums;

namespace AmoCrm.Api.Common;

public record NoteOrderModel
{
    public NoteOrderEnum OrderField { get; init; }
    public OrderEnum Order { get; init; }
}
