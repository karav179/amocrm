﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Common;

public record LinksResponse
{
    [JsonProperty("self")]
    public HrefResponse Self { get; init; } = null!;

    [JsonProperty("next")]
    public HrefResponse? Next { get; init; }

    [JsonProperty("first")]
    public HrefResponse? First { get; init; }

    [JsonProperty("prev")]
    public HrefResponse? Prev { get; init; }
}
