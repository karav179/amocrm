﻿namespace AmoCrm.Api.Common;

public record PeriodModel
{
    public DateTimeOffset? FromDate { get; init; }
    public DateTimeOffset? ToDate { get; init; }

    public long? FromTicks => FromDate.HasValue ? FromDate.Value.ToUnixTimeSeconds() : null;
    public long? ToTicks => ToDate.HasValue ? ToDate.Value.ToUnixTimeSeconds() : null;
}
