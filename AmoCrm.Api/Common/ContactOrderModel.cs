﻿using AmoCrm.Api.Enums;

namespace AmoCrm.Api.Common;

public record ContactOrderModel
{
    public ContactOrderEnum OrderField { get; init; }
    public OrderEnum Order { get; init; }
}
