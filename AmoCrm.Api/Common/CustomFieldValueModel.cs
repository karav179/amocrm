﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Common;

public record CustomFieldValueModel
{
    [JsonProperty("value")]
    public string Value { get; init; } = string.Empty;

    [JsonProperty("enum_id")]
    public int? EnumId { get; init; }

    [JsonProperty("enum_code")]
    public string? EnumCode { get; init; }
}
