﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Common;

public record TaskTextModel
{
    [JsonProperty("text")]
    public string Text { get; init; } = string.Empty;
}
