﻿using AmoCrm.Api.Enums;

namespace AmoCrm.Api.Common;

public record TaskEntityFilterModel
{
    public TaskEntityTypeFilterEnum EntityType { get; init; }
    public int[] EntityIds { get; init; } = Array.Empty<int>();
}
