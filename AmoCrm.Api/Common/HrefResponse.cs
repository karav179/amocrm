﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Common;

public record HrefResponse
{
    [JsonProperty("href")]
    public string Href { get; init; } = string.Empty;
}
