﻿namespace AmoCrm.Api.Common;

public record StatusFilterModel
{
    public int PipelineId { get; init; }
    public int StatusId { get; init; }
}
