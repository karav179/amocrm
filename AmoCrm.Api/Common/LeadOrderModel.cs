﻿using AmoCrm.Api.Enums;

namespace AmoCrm.Api.Common;

public record LeadOrderModel
{
    public LeadOrderEnum OrderField { get; init; }
    public OrderEnum Order { get; init; }
}
