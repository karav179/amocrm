﻿namespace AmoCrm.Api.Common;

public record CustomFieldRequest
{
    public string FieldId { get; init; } = string.Empty;
    public string Value { get; init; } = string.Empty;
}
