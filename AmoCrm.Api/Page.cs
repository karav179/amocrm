﻿namespace AmoCrm.Api;

public class Page<T> where T : class
{
    public int PageNum { get; set; }

    public T[] Resumes { get; set; } = Array.Empty<T>();
}

