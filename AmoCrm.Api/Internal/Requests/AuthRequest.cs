﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Internal.Requests;

internal record AuthRequest
{
    [JsonProperty("grant_type")]
    public string GrantType => string.IsNullOrEmpty(RefreshToken) ? "authorization_code" : "refresh_token";

    [JsonProperty("client_id")]
    public required string ClientdId { get; init; }

    [JsonProperty("client_secret")]
    public required string ClientSecret { get; init; } = string.Empty;

    [JsonProperty("redirect_uri")]
    public required string RedirectUri { get; init; } = string.Empty;

    [JsonProperty("refresh_token")]
    public string? RefreshToken { get; set; }

    [JsonProperty("code")]
    public string? AuthorizationCode { get; set; }
}