﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Internal.Responses;

public abstract record BaseArrayResponse
{
    [JsonProperty("_page")]
    public int Page { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
