﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Internal.Responses;

internal record AuthResponse
{
    [JsonProperty("token_type")]
    public required string TokenType { get; init; }

    [JsonProperty("expires_in")]
    public int ExpiresIn { get; init; }

    [JsonProperty("access_token")]
    public required string AccessToken { get; init; }

    [JsonProperty("refresh_token")]
    public required string RefreshToken { get; init; }
}
