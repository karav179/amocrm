﻿namespace AmoCrm.Api.Internal.Converters;

internal static class UnixTimestampConverter
{
    internal static DateTime Convert(long unixTimestamp)
    {
        var defaultDate = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        try
        {
            defaultDate = defaultDate.AddSeconds(unixTimestamp);
        }
        catch { }

        return defaultDate;
    }
}
