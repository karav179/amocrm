﻿using AmoCrm.Api.Internal.Responses;

namespace AmoCrm.Api;

public class OperationEnumerator<T> where T : BaseArrayResponse?
{
    private readonly Func<string, CancellationToken, Task<T>> _callback;
    private string _query;
    private readonly CancellationToken _ct;

    private bool _isNextDataExists = true;

    public OperationEnumerator(Func<string, CancellationToken, Task<T>> callback, string query, CancellationToken ct)
    {
        _callback = callback;
        _query = query;
        _ct = ct;
    }

    public T? Current { get; private set; }

    public async ValueTask<bool> MoveNextAsync()
    {
        _ct.ThrowIfCancellationRequested();

        if (!_isNextDataExists)
            return false;

        Current = await _callback(_query, _ct);

        _isNextDataExists = Current?.Links.Next is not null;
        if (_isNextDataExists)
        {
            var uri = new Uri(Current!.Links.Next!.Href);
            _query = uri.Query;
        }

        return true;
    }

    public ValueTask DisposeAsync()
    {
        return new ValueTask(Task.CompletedTask);
    }
}
